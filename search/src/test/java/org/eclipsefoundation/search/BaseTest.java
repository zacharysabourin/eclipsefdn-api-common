/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.search;

import javax.enterprise.inject.Produces;

import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.dao.impl.PlaceholderPersistenceDao;
import org.junit.jupiter.api.Test;

import io.quarkus.arc.DefaultBean;
import io.quarkus.test.junit.QuarkusTest;

/**
 * Forces the application to start and compile a dunning test instance to
 * validate the server can start up with bare configurations (which it should).
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class BaseTest {

	@Test
	void running() {
	}

	/**
	 * Required as there are no DB beans configured in the search package. This
	 * allows us to by default provide the placeholder DAO, which is removed by
	 * Quarkus regardless of the unremovable property.
	 * 
	 * @return
	 */
	@Produces
	@DefaultBean
	public PersistenceDao defaultTestingPersistenceDao() {
		return new PlaceholderPersistenceDao();
	}
}
