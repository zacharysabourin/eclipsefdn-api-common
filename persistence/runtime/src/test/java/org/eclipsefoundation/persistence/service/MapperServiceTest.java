/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.service;

import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.inject.Inject;

import org.eclipsefoundation.persistence.test.dto.PersonDTO;
import org.eclipsefoundation.persistence.test.model.PersonModel;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Tests the MapperService by attempting to retrieve a Mapper by model and DTO.
 * Tests that the retrieved Mapper converts properly in both directions.
 * 
 * @author Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
 */
@QuarkusTest
public class MapperServiceTest {

    @Inject
    MapperService<PersonDTO, PersonModel> mapperService;

    @Test
    public void getMapperByModel() {
        assertTrue(mapperService.getByModel(PersonModel.class) != null);
        assertTrue(mapperService.getByModel(PersonModel.class).getDTOType() == PersonDTO.class);
    }

    @Test
    public void getMapperByDTO() {
        assertTrue(mapperService.getByDTO(PersonDTO.class) != null);
        assertTrue(mapperService.getByDTO(PersonDTO.class).getModelType() == PersonModel.class);
    }    

    @Test
    public void convertToDTO_mapperByModel() {
        PersonModel personModel = PersonModel.builder().setAge(25).setName("JimBob").setPersonID("TM-50").build();

        PersonDTO personDTO = mapperService.getByModel(PersonModel.class).toDTO(personModel);

        assertTrue(personModel.getPersonID().equals(personDTO.getId()));
        assertTrue(personModel.getName().equals(personDTO.getName()));
        assertTrue(personModel.getAge() == personDTO.getAge());
    }

    @Test
    public void convertToDTO_mapperByDTO() {
        PersonModel personModel = PersonModel.builder().setAge(25).setName("JimBob").setPersonID("TM-50").build();

        PersonDTO personDTO = mapperService.getByDTO(PersonDTO.class).toDTO(personModel);

        assertTrue(personModel.getPersonID().equals(personDTO.getId()));
        assertTrue(personModel.getName().equals(personDTO.getName()));
        assertTrue(personModel.getAge() == personDTO.getAge());
    }

    @Test
    public void convertToModel_mapperbyModel() {

        PersonDTO personDTO = new PersonDTO();
        personDTO.setPersonID("TM-50");
        personDTO.setName("JimBob");
        personDTO.setAge(25);

        PersonModel personModel = mapperService.getByModel(PersonModel.class).toModel(personDTO);

        assertTrue(personModel.getPersonID().equals(personDTO.getId()));
        assertTrue(personModel.getName().equals(personDTO.getName()));
        assertTrue(personModel.getAge() == personDTO.getAge());
    }

    @Test
    public void convertToModel_mapperbyDTO() {

        PersonDTO personDTO = new PersonDTO();
        personDTO.setPersonID("TM-50");
        personDTO.setName("JimBob");
        personDTO.setAge(25);

        PersonModel personModel = mapperService.getByDTO(PersonDTO.class).toModel(personDTO);

        assertTrue(personModel.getPersonID().equals(personDTO.getId()));
        assertTrue(personModel.getName().equals(personDTO.getName()));
        assertTrue(personModel.getAge() == personDTO.getAge());
    }
}
