/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.test.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

import org.eclipsefoundation.persistence.dto.BareNode;

/**
 * A basic DTO class with a few simple fields. Used for testing.
 */
public class PersonDTO extends BareNode implements Serializable {

    @Id
    @Column(unique = true, nullable = false)
    private String personID;
    private String name;
    private int age;

    @Override
    public Object getId() {
        return getPersonID();
    }

    public String getPersonID() {
        return this.personID;
    }

    public void setPersonID(String id) {
        this.personID = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}