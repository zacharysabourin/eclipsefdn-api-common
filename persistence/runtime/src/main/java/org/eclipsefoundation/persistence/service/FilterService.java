/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.service;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;

/**
 * Provides filters for the passed class reference to reduce the number of injections that need to
 * be handled by Graph endpoints.
 *
 * @author Martin Lowe
 */
public interface FilterService {

  /**
   * Returns a DTO filter if available for the given class.
   *
   * @param <T> the type of entity being filtered
   * @param target class ref for the target entity type
   * @return a filter instance for the given class if it exists, null otherwise
   */
  <T extends BareNode> DtoFilter<T> get(Class<T> target);
}
