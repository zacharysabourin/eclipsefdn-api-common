/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.model;

/**
 * Special use case SQL statement that allows for direct execution of SQL with minimal changes/modifications. Ordinals
 * are still substituted, but SQL is directly set by callStatement variable.
 */
public class ParameterizedCallStatement extends ParameterizedSQLStatement {
    private String callStatement;

    ParameterizedCallStatement(DtoTable base, SQLGenerator gen) {
        super(base, gen);
    }

    @Override
    public String getSelectSql() {
        throw new IllegalStateException("Cannot use select when referencing stored procedures");
    }

    @Override
    public String getCountSql() {
        throw new IllegalStateException("Cannot use count when referencing stored procedures");
    }

    /**
     * Set thet call statement to be executed.
     * 
     * @param callStatement
     */
    public void setCallStatement(String callStatement) {
        this.callStatement = callStatement;
    }

    public String getCallStatement() {
        return this.callStatement;
    }
}
