/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.dao.impl;

import java.util.List;

import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.RDBMSQuery;

/**
 * Produces a bare, non-functioning DAO for downstream build dependencies. This
 * enables downstream package builds when there are no available JPA entities.
 * 
 * @author Martin Lowe
 *
 */
public class PlaceholderPersistenceDao implements PersistenceDao {

	@Override
	public HealthCheckResponse call() {
		throw new IllegalStateException("Placeholder DAO should not be used in running instances");
	}

	@Override
	public <T extends BareNode> List<T> get(RDBMSQuery<T> q) {
		throw new IllegalStateException("Placeholder DAO should not be used in running instances");
	}

	@Override
	public <T extends BareNode> List<T> add(RDBMSQuery<T> q, List<T> documents) {
		throw new IllegalStateException("Placeholder DAO should not be used in running instances");

	}

	@Override
	public <T extends BareNode> void delete(RDBMSQuery<T> q) {
		throw new IllegalStateException("Placeholder DAO should not be used in running instances");
	}

	@Override
	public Long count(RDBMSQuery<?> q) {
		throw new IllegalStateException("Placeholder DAO should not be used in running instances");
	}

	@Override
	public <T extends BareNode> T getReference(Object id, Class<T> type) {
		throw new IllegalStateException("Placeholder DAO should not be used in running instances");
	}

}
