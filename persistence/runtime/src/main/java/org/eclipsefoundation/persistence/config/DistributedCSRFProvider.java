/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.config;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.eclipsefoundation.core.model.CSRFGenerator;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.dto.DistributedCSRFToken.DistributedCSRFTokenFilter;
import org.eclipsefoundation.persistence.model.DistributedCSRFGenerator;

import io.quarkus.arc.Priority;
import io.quarkus.arc.properties.IfBuildProperty;
import io.quarkus.arc.properties.UnlessBuildProperty;

public class DistributedCSRFProvider {

    @Produces
    @Priority(5)
    @ApplicationScoped
    @IfBuildProperty(name = "security.csrf.enabled.distributed-mode", stringValue = "true")
    @UnlessBuildProperty(name = "security.csrf.distributed-mode.default-provider", stringValue = "false", enableIfMissing = true)
    public CSRFGenerator distributedGenerator(DefaultHibernateDao defaultDao, DistributedCSRFTokenFilter filter) {
        return new DistributedCSRFGenerator(defaultDao, filter);
    }
}
