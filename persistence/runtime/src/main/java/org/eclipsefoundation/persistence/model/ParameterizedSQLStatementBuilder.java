/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.model;

public class ParameterizedSQLStatementBuilder {

    SQLGenerator generator;

    public ParameterizedSQLStatementBuilder(SQLGenerator generator) {
        this.generator = generator;
    }

    public ParameterizedSQLStatement build(DtoTable base) {
        return new ParameterizedSQLStatement(base, generator);
    }

    public ParameterizedCallStatement buildCallStatement(DtoTable base) {
        return new ParameterizedCallStatement(base, generator);
    }
}
