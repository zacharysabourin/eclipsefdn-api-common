/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.dto;

import java.time.ZonedDateTime;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DistributedCSRFGenerator;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table
public class DistributedCSRFToken extends BareNode {
    public static final DtoTable TABLE = new DtoTable(DistributedCSRFToken.class, "dct");

    @Id
    private String token;
    private String userAgent;
    private String ipAddress;
    private String user;
    private ZonedDateTime timestamp;

    @Override
    @JsonIgnore
    public Object getId() {
        return getToken();
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the userAgent
     */
    public String getUserAgent() {
        return userAgent;
    }

    /**
     * @param userAgent the userAgent to set
     */
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    /**
     * @return the ipAddress
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * @param ipAddress the ipAddress to set
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the timestamp
     */
    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Singleton
    public static class DistributedCSRFTokenFilter implements DtoFilter<DistributedCSRFToken> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // IP check
                String ip = params.getFirst(DistributedCSRFGenerator.IP_PARAM);
                if (ip != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".ipAddress = ?",
                            new Object[] { ip }));
                }
                // UA check
                String ua = params.getFirst(DistributedCSRFGenerator.USERAGENT_PARAM);
                if (ua != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".userAgent = ?",
                            new Object[] { ua }));
                }
                // user check
                String user = params.getFirst(DistributedCSRFGenerator.USER_PARAM);
                if (user != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".user = ?",
                            new Object[] { user }));
                }
            }

            return stmt;
        }

        @Override
        public Class<DistributedCSRFToken> getType() {
            return DistributedCSRFToken.class;
        }
    }
}
