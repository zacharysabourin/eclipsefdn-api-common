/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.service;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.mapper.EntityMapper;

/**
 * MapperService interface to allow retrieval of a corresponding EntityMapper.
 * Can retrieve an EntityMapper based on target DTO or model class.
 * 
 * @author Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
 */
public interface MapperService<T extends BareNode, S> {

    /**
     * Method to allow retrieval of EntityMapper that corresponds to the target
     * model class.
     * 
     * @param target The target class used for EntityMapper retrieval
     * @return A reference to the target EntityMapper
     */
    EntityMapper<T, S> getByModel(Class<S> target);

    /**
     * Method to allow retrieval of EntityMapper that corresponds to the target
     * DTO class.
     * 
     * @param target The target class used for EntityMapper retrieval
     * @return A reference to the target EntityMapper
     */
    EntityMapper<T, S> getByDTO(Class<T> target);
}
