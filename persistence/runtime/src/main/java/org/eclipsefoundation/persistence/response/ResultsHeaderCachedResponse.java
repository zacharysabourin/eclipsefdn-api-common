/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.response;

import java.net.URI;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.helper.ParameterHelper;
import org.eclipsefoundation.core.model.CacheRecorder;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.core.response.PaginatedResultsFilter;
import org.jboss.resteasy.core.ResteasyContext;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Records the results and page size headers to be reapplied to cached results.
 * 
 * @author Martin Lowe
 *
 */
public class ResultsHeaderCachedResponse extends CacheRecorder<MultivaluedMap<String, String>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResultsHeaderCachedResponse.class);

    @Inject
    ParameterHelper paramHelper;

    @Override
    protected MultivaluedMap<String, String> calculateRecord(RequestWrapper context) {
        HttpServletResponse response = ResteasyContext.getContextData(HttpServletResponse.class);
        MultivaluedMap<String, String> out = new MultivaluedMapImpl<>();
        // if response is null, tracking request outside of request context. Do best efforts tracking w/ context
        if (response == null) {
            LOGGER.debug("Attempting recording for request made outside of RequestContext: {}", context.getEndpoint());
            out.add(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER,
                    context.getResponseHeader(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
            out.add(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER,
                    context.getResponseHeader(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
            LOGGER.debug("Storing keys {}({}) and {}({})", PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER,
                    context.getResponseHeader(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER),
                    PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER,
                    context.getResponseHeader(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        } else {
            out.add(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER,
                    getContextBackedResponseHeader(response, context, PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
            out.add(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER,
                    getContextBackedResponseHeader(response, context, PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));

            LOGGER.debug("Storing keys {}({}) and {}({})", PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER,
                    out.getFirst(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER),
                    PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER,
                    out.getFirst(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        }
        return out;
    }

    @Override
    public void apply(MultivaluedMap<String, String> record, RequestWrapper context,
            ContainerResponseContext responseContext) {
        LOGGER.trace("Reapplying keys {}", record.keySet());
        record.forEach((key, values) -> {
            // null check the lists and only use first value as its whats expected
            if (values != null) {
                responseContext.getHeaders().add(key, values.get(0));
            }
        });
    }

    @Override
    public String generateKey(RequestWrapper context) {
        StringBuilder sb = new StringBuilder();
        URI uri = context.getURI();
        if (uri != null) {
            sb.append(uri.getPath());
        }
        MultivaluedMap<String, String> params = context.asMap();
        // filter out the pagination headers as the results should be the same for any page
        params.remove(DefaultUrlParameterNames.PAGE.getName());
        params.remove(DefaultUrlParameterNames.PAGESIZE.getName());
        paramHelper.filterUnknownParameters(params).entrySet().stream().filter(e -> !e.getValue().isEmpty())
                .map(e -> e.getKey() + '=' + String.join(",", e.getValue())).forEach(s -> sb.append('|').append(s));
        return sb.toString();

    }

    private String getContextBackedResponseHeader(HttpServletResponse response, RequestWrapper context,
            String headerName) {
        return response.containsHeader(headerName) && StringUtils.isNotBlank(response.getHeader(headerName))
                ? response.getHeader(headerName)
                : context.getHeader(headerName);
    }
}
