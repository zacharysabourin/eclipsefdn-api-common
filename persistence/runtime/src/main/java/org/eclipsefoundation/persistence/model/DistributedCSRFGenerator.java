/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.model;

import java.net.URI;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.model.CSRFGenerator.DefaultCSRFGenerator;
import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.dto.DistributedCSRFToken;
import org.eclipsefoundation.persistence.dto.DistributedCSRFToken.DistributedCSRFTokenFilter;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.undertow.httpcore.HttpHeaderNames;

/**
 * Using IP address and useragent, create a fingerprint for the current user that will be used to access a stored
 * distributed CSRF token. If one does not yet exist, one will be generated using the default method, stored for future
 * reference, and returned.
 * 
 * @author Martin Lowe
 *
 */
public class DistributedCSRFGenerator extends DefaultCSRFGenerator {
    public static final Logger LOGGER = LoggerFactory.getLogger(DistributedCSRFGenerator.class);

    public static final String USER_PARAM = "user";
    public static final String USERAGENT_PARAM = "useragent";
    public static final String IP_PARAM = "ip";

    private PersistenceDao manager;
    private DistributedCSRFTokenFilter filter;

    public DistributedCSRFGenerator(PersistenceDao manager, DistributedCSRFTokenFilter filter) {
        LOGGER.info("Distributed CSRF strategy is enabled.");
        this.manager = manager;
        this.filter = filter;
    }

    @Override
    public String getCSRFToken(HttpServletRequest httpServletRequest) {
        // generate a non-root query
        MultivaluedMap<String, String> params = getQueryParameters(httpServletRequest);
        RDBMSQuery<DistributedCSRFToken> q = new RDBMSQuery<>(
                new FlatRequestWrapper(URI.create(httpServletRequest.getRequestURL().toString())), filter, params);
        q.setRoot(false);
        // attempt to read existing tokens
        List<DistributedCSRFToken> tokens = manager.get(q);
        if (tokens.isEmpty()) {
            // generate a new token to be stored in the distributed persistence table
            String token = super.getCSRFToken(httpServletRequest);
            DistributedCSRFToken t = new DistributedCSRFToken();
            t.setIpAddress(params.getFirst(IP_PARAM));
            t.setToken(token);
            t.setUser(params.getFirst(USER_PARAM));
            t.setUserAgent(params.getFirst(USERAGENT_PARAM));
            t.setTimestamp(DateTimeHelper.now());
            List<DistributedCSRFToken> createdTokens = manager.add(q, Arrays.asList(t));
            if (!createdTokens.isEmpty()) {
                LOGGER.trace("Generated distributed CSRF token for current fingerprint; IP: {}, UA: {}, token: {}",
                        t.getIpAddress(), t.getUserAgent(), token);
                return token;
            }
        } else {
            DistributedCSRFToken t = tokens.get(0);
            LOGGER.trace("Found existing distributed CSRF token for current fingerprint; IP: {}, UA: {}, token: {}",
                    t.getIpAddress(), t.getUserAgent(), t.getToken());
            return t.getToken();
        }
        LOGGER.info("Error while generating/retrieving CSRF entry for current user; IP: {}, UA: {}",
                params.getFirst(IP_PARAM), params.getFirst(USERAGENT_PARAM));
        return null;
    }

    public void destroyCurrentToken(HttpServletRequest httpServletRequest) {
        MultivaluedMap<String, String> params = getQueryParameters(httpServletRequest);
        LOGGER.error("Destroying retrieving CSRF entry for current user; IP: {}, UA: {}", params.getFirst(IP_PARAM),
                params.getFirst(USERAGENT_PARAM));
        manager.delete(new RDBMSQuery<>(
                new FlatRequestWrapper(URI.create(httpServletRequest.getRequestURL().toString())), filter, params));
    }

    private MultivaluedMap<String, String> getQueryParameters(HttpServletRequest httpServletRequest) {
        // get the markers used to identify a user (outside of a unique session ID)
        String ipAddr = getClientIpAddress(httpServletRequest);
        String userAgent = httpServletRequest.getHeader(HttpHeaderNames.USER_AGENT);
        Principal user = httpServletRequest.getUserPrincipal();
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(IP_PARAM, ipAddr);
        params.add(USERAGENT_PARAM, userAgent);
        params.add(USER_PARAM, user != null ? user.getName() : null);
        return params;
    }

    private String getClientIpAddress(HttpServletRequest request) {
        String forwardedFor = request.getHeader(HttpHeaderNames.X_FORWARDED_FOR);
        if (forwardedFor == null) {
            return request.getRemoteAddr();
        } else {
            // get the first most address in forwarded chain
            return new StringTokenizer(forwardedFor, ",").nextToken().trim();
        }
    }
}
