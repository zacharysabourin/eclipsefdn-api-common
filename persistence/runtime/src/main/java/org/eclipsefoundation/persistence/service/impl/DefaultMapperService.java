/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.service.impl;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.mapper.EntityMapper;
import org.eclipsefoundation.persistence.service.MapperService;

/**
 * Indicates an entity mapping between DTOs and models
 *
 * @author Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
 * 
 * @param <T> the DTO object in the mapping pair
 * @param <S> the model object in the mapping pair
 */
@ApplicationScoped
public class DefaultMapperService<T extends BareNode, S> implements MapperService<T, S> {

    @Inject
    Instance<EntityMapper<?, ?>> mappers;

    /**
     * Retrieves an instance of EntityMapper that corresponds to the target model
     * class.
     * 
     * @param target The target class used for EntityMapper retrieval
     * @return A reference to the target EntityMapper if it exists
     */
    @Override
    @SuppressWarnings("unchecked")
    public EntityMapper<T, S> getByModel(Class<S> target) {
        return (EntityMapper<T, S>) mappers.stream().filter(map -> map.getModelType().equals(target))
                .findFirst().orElse(null);
    }

    /**
     * Retrieves an instance of EntityMapper that corresponds to the target DTO
     * class.
     * 
     * @param target The target class used for EntityMapper retrieval
     * @return A reference to the target EntityMapper if it exists
     */
    @Override
    @SuppressWarnings("unchecked")
    public EntityMapper<T, S> getByDTO(Class<T> target) {
        return (EntityMapper<T, S>) mappers.stream().filter(map -> map.getDTOType().equals(target))
                .findFirst().orElse(null);
    }
}
