/*********************************************************************
* Copyright (c) 2019 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import org.eclipsefoundation.core.exception.MaintenanceException;
import org.eclipsefoundation.core.response.PaginatedResultsFilter;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.ParameterizedCallStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement.Clause;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation of the DB DAO, persisting via the Hibernate framework.
 * 
 * @author Martin Lowe
 */
public abstract class BaseHibernateDao implements PersistenceDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseHibernateDao.class);

    @Inject
    EntityManager primaryConnectionManager;

    @Override
    public <T extends BareNode> List<T> get(RDBMSQuery<T> q) {
        if (isMaintenanceMode()) {
            throw new MaintenanceException();
        }
        // set up the query, either for stored procedures or standard SQL
        Query query;
        if (q.getFilter() instanceof ParameterizedCallStatement) {
            query = getSecondaryEntityManager()
                    .createStoredProcedureQuery(((ParameterizedCallStatement) q.getFilter()).getCallStatement(), q.getDocType());
        } else {
            query = getSecondaryEntityManager().createQuery(q.getFilter().getSelectSql(), q.getDocType());
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Querying DB using the following query: {}", q.getFilter().getSelectSql());
            }

            // handle root request parameters (setting headers to track limit and max count)
            if (q.isRoot()) {
                handleRoot(q);
            }
            // check if result set should be limited
            if (q.getDTOFilter().useLimit() && q.useLimit()) {
                LOGGER.debug("Querying DB using offset of {} and limit of {}", getOffset(q), getLimit(q));
                query = query.setFirstResult(getOffset(q)).setMaxResults(getLimit(q));
            }
        }

        // add ordinal parameters
        int ord = 1;
        for (Clause c : q.getFilter().getClauses()) {
            for (Object param : c.getParams()) {
                // for each of params, if processing call statement, add params as registered params
                if (q.getFilter() instanceof ParameterizedCallStatement) {
                    ((StoredProcedureQuery) query).registerStoredProcedureParameter(ord, param.getClass(), ParameterMode.IN);
                }
                query.setParameter(ord++, param);
            }
        }
        // run the query
        return castResultsToType(query.getResultList(), q.getDocType());
    }

    @Transactional
    @Override
    public <T extends BareNode> List<T> add(RDBMSQuery<T> q, List<T> documents) {
        if (isMaintenanceMode()) {
            throw new MaintenanceException();
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Adding {} documents to DB of type {}", documents.size(), q.getDocType().getSimpleName());
        }
        EntityManager em = getPrimaryEntityManager();
        // for each doc, check if update or create
        List<T> updatedDocs = new ArrayList<>(documents.size());
        for (T doc : documents) {
            T ref = doc;
            if (doc.getId() != null) {
                // ensure this object exists before merging on it
                if (em.find(q.getDocType(), doc.getId()) != null) {
                    LOGGER.debug("Merging document with existing document with id '{}'", doc.getId());
                    ref = em.merge(doc);
                } else {
                    LOGGER.debug("Persisting new document with id '{}'", doc.getId());
                    em.persist(doc);
                }
            } else {
                LOGGER.debug("Persisting new document with generated UUID ID");
                em.persist(doc);
            }
            // add the ref to the output list
            updatedDocs.add(ref);
        }
        return updatedDocs;
    }

    @Transactional
    @Override
    public <T extends BareNode> void delete(RDBMSQuery<T> q) {
        if (isMaintenanceMode()) {
            throw new MaintenanceException();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Removing documents from DB using the following query: {}", q);
        }
        // retrieve results for the given deletion query to delete using entity manager
        EntityManager em = getPrimaryEntityManager();
        List<T> results = get(q);
        if (results != null) {
            // remove all matched documents
            results.forEach(em::remove);
        }
    }

    @Transactional
    @Override
    public Long count(RDBMSQuery<?> q) {
        if (isMaintenanceMode()) {
            throw new MaintenanceException();
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Counting documents in DB that match the following query: {}", q.getFilter().getCountSql());
        }
        EntityManager em = getSecondaryEntityManager();

        // build base query
        TypedQuery<Long> query = em.createQuery(q.getFilter().getCountSql(), Long.class);
        // add ordinal parameters
        int ord = 1;
        for (Clause c : q.getFilter().getClauses()) {
            for (Object param : c.getParams()) {
                query.setParameter(ord++, param);
            }
        }
        return query.getSingleResult();
    }

    @Override
    public <T extends BareNode> T getReference(Object id, Class<T> type) {
        if (isMaintenanceMode()) {
            throw new MaintenanceException();
        }
        return getSecondaryEntityManager().getReference(type, id);
    }

    /**
     * Handles operations that should happen on a "root" or main DB request for a fulfilled request. This is done to ensure
     * that values such as DB max are abided.
     */
    private void handleRoot(RDBMSQuery<?> q) {
        // check if count has been performed, if not do so and set it to the response
        q.getWrapper().setHeader(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, Long.toString(count(q)));
        q.getWrapper().setHeader(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, Integer.toString(getLimit(q)));
    }

    private int getLimit(RDBMSQuery<?> q) {
        return q.getLimit() > 0 ? Math
                .min(q.getLimit(),
                        ConfigProvider.getConfig().getOptionalValue("eclipse.db.default.limit", Integer.class).orElseGet(() -> 1000))
                : ConfigProvider.getConfig().getOptionalValue("eclipse.db.default.limit.max", Integer.class).orElseGet(() -> 1000);
    }

    private int getOffset(RDBMSQuery<?> q) {
        // allow for manual offsetting
        int manualOffset = q.getManualOffset();
        if (manualOffset > 0) {
            return manualOffset;
        }
        // if first page, no offset
        if (q.getPage() <= 1) {
            return 0;
        }
        int limit = getLimit(q);
        return (limit * q.getPage()) - limit;
    }

    /**
     * Checked casting of results to properly typed list.
     * 
     * @param <T> the type to cast the list to
     * @param l the list that needs to be cast to a type
     * @param clazz the literal type to cast to
     * @return the original list cast to the passed type
     * @throws ClassCastException if the internal types do not match the passed type.
     */
    private <T> List<T> castResultsToType(List<?> l, Class<T> clazz) {
        return l.stream().map(clazz::cast).collect(Collectors.toList());
    }

    @Override
    public HealthCheckResponse call() {
        HealthCheckResponseBuilder b = HealthCheckResponse.named("DB readiness");
        if (isMaintenanceMode()) {
            return b.down().withData("error", "Maintenance flag is set").build();
        }
        return b.up().build();
    }

    protected boolean isMaintenanceMode() {
        return ConfigProvider.getConfig().getOptionalValue("eclipse.db.maintenance", Boolean.class).orElseGet(() -> false);
    }

    /**
     * Allows for retrieval of multi-tenant persistence sessions. This is needed as sessions are generated based on
     * annotated PersistenceUnit values.
     * 
     * @return entity manager object linked to current transaction
     */
    protected EntityManager getPrimaryEntityManager() {
        return primaryConnectionManager;
    }

    /**
     * Allow for multiple connections to be associated with a datasource for separate read/write strategies. The secondary
     * connection would be a read-only interface to reduce traffic on primary write enable database replicants. Default
     * behaviour returns the the primary entity manager but this can be overriden.
     * 
     * @return the secondary entity manager, defaults to the primary entity manager.
     */
    protected EntityManager getSecondaryEntityManager() {
        return getPrimaryEntityManager();
    }
}
