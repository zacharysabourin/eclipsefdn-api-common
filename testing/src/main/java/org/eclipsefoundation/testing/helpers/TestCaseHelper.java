/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.testing.helpers;

import java.util.Optional;

import org.eclipsefoundation.testing.templates.RestAssuredTemplates.EndpointTestCase;

import io.restassured.http.ContentType;

/**
 * Provides simple methods to build a test case with the desired parameters.
 * 
 * @author Zachary Sabourin
 */
public class TestCaseHelper {

    /**
     * Creates a EndpointTestCase object with the desired path, params, and schema
     * location.
     * Used to test successful requests/reponses. Status code 200
     * (ok) is assumed.
     * 
     * @param path           the endpoint URL
     * @param params         the request path/query parameters
     * @param schemaLocation the schema location
     * @return the desired endpoint test case
     */
    public static EndpointTestCase buildSuccessCase(String path, String[] params, String schemaLocation) {
        return prepareTestCase(path, params, schemaLocation).build();
    }

    /**
     * Creates an EndpointTestCase.builder to allow the user to add any extra test
     * parameters. As well as overwrite any of the default success case parameters.
     * 
     * @param path           the endpoint URL
     * @param params         the request path/query parameters
     * @param schemaLocation the schema location
     * @return the desired endpoint test case builder
     */
    public static EndpointTestCase.Builder prepareTestCase(String path, String[] params, String schemaLocation) {
        return EndpointTestCase.builder()
                .setPath(path)
                .setParams(Optional.of(params))
                .setResponseContentType(ContentType.JSON)
                .setSchemaLocation(schemaLocation);
    }

    /**
     * Creates a EndpointTestCase object with the desired path and params.
     * Used to test a bad request. Status code 400 (Bad Request) is assumed.
     * 
     * @param path   the endpoint URL
     * @param params the request path/query parameters
     * @return the desired endpoint test case
     */
    public static EndpointTestCase buildBadRequestCase(String path, String[] params, String schemaLocation) {
        return EndpointTestCase.builder()
                .setPath(path)
                .setParams(Optional.of(params))
                .setSchemaLocation(schemaLocation)
                .setStatusCode(400)
                .build();
    }

    /**
     * Creates a EndpointTestCase object with the desired path and params.
     * Used to test an forbidden request. Status code 403 (Forbidden) is
     * assumed.
     * 
     * @param path   the endpoint URL
     * @param params the request path/query parameters
     * @return the desired endpoint test case
     */
    public static EndpointTestCase buildForbiddenCase(String path, String[] params, String schemaLocation) {
        return EndpointTestCase.builder()
                .setPath(path)
                .setParams(Optional.of(params))
                .setSchemaLocation(schemaLocation)
                .setStatusCode(403)
                .build();
    }

    /**
     * Creates a EndpointTestCase object with the desired path and params.
     * Used to test a URL that doesn't exist. Status code 404 (Not Found) is
     * assumed.
     * 
     * @param path   the endpoint URL
     * @param params the request path/query parameters
     * @return the desired endpoint test case
     */
    public static EndpointTestCase buildNotFoundCase(String path, String[] params, String schemaLocation) {
        return EndpointTestCase.builder()
                .setPath(path)
                .setParams(Optional.of(params))
                .setSchemaLocation(schemaLocation)
                .setStatusCode(404)
                .build();
    }

    /**
     * Creates a EndpointTestCase object with the desired path, params, and request
     * format.
     * Used to test an invalid request body format. Status code
     * 500 (Internal Error) is assumed.
     * 
     * @param path          the endpoint URL
     * @param params        the request path/query parameters
     * @param invalidFormat the invalid request format
     * @return the desired endpoint test case
     */
    public static EndpointTestCase buildInvalidFormatCase(String path, String[] params, ContentType invalidFormat) {
        return EndpointTestCase.builder()
                .setPath(path)
                .setParams(Optional.of(params))
                .setRequestContentType(invalidFormat)
                .setStatusCode(500)
                .build();
    }

    private TestCaseHelper() {
    }
}
