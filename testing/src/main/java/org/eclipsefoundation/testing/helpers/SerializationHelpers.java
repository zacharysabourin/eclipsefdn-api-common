/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.testing.helpers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Provides serialization helper methods. These method allow encoding values to
 * various types.
 */
public class SerializationHelpers {

    /**
     * GZIPs the input string and returns a ByteArrayInputStream containgin the
     * content.
     * 
     * @param content The String to convert
     * @return A ByteArrayInputStream
     */
    public static ByteArrayInputStream writeAsGZIP(String content) {
        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
                GZIPOutputStream gzipOut = new GZIPOutputStream(byteOut);) {

            gzipOut.write(content.getBytes());
            gzipOut.close();

            return new ByteArrayInputStream(byteOut.toByteArray());

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
