/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*	      Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.testing.templates;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.is;

import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.annotation.Nullable;
import javax.ws.rs.HttpMethod;

import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipsefoundation.core.namespace.MicroprofilePropertyNames;
import org.eclipsefoundation.testing.helpers.AuthHelper;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

/**
 * Provides some basic testing templates to cover majority of test cases and
 * take care of things like CSRF, format, and
 * makes the whole thing reusable.
 * 
 * @author Martin Lowe
 * @author Zachary sabourin
 */
public final class RestAssuredTemplates {

    /*
     * GET TESTS
     */
    public static void testGet(EndpointTestCase testCase) {
        getValidatedResponse(testCase, HttpMethod.GET).statusCode(testCase.getStatusCode());
    }

    public static void testGet_validateSchema(EndpointTestCase testCase) {
        getValidatedResponse(testCase, HttpMethod.GET).assertThat()
                .body(matchesJsonSchemaInClasspath(testCase.getSchemaLocation()));
    }

    public static void testGet_validateResponseFormat(EndpointTestCase testCase) {
        getValidatedResponse(testCase, HttpMethod.GET).assertThat()
                .contentType(testCase.getResponseContentType())
                .statusCode(testCase.getStatusCode());
    }

    /*
     * POST TESTS
     */
    public static void testPost(EndpointTestCase testCase, Object body) {
        processResponseBody(getValidatedResponse(testCase, HttpMethod.POST, body)
                .statusCode(testCase.getStatusCode()), testCase.getBodyValidationParams());
    }

    public static void testPost_validateSchema(EndpointTestCase testCase, Object body) {
        getValidatedResponse(testCase, HttpMethod.POST, body).assertThat()
                .body(matchesJsonSchemaInClasspath(testCase.getSchemaLocation()));
    }

    public static void testPost_validateResponseFormat(EndpointTestCase testCase, Object body) {
        getValidatedResponse(testCase, HttpMethod.POST, body).assertThat()
                .contentType(testCase.getResponseContentType())
                .statusCode(testCase.getStatusCode());
    }

    /*
     * PUT TESTS
     */
    public static void testPut(EndpointTestCase testCase, Object body) {
        processResponseBody(getValidatedResponse(testCase, HttpMethod.PUT, body).statusCode(testCase.getStatusCode()),
                testCase.getBodyValidationParams());
    }

    public static void testPut_validateSchema(EndpointTestCase testCase, Object body) {
        getValidatedResponse(testCase, HttpMethod.PUT, body).assertThat()
                .body(matchesJsonSchemaInClasspath(testCase.getSchemaLocation()));
    }

    public static void testPut_validateResponseFormat(EndpointTestCase testCase, Object body) {
        getValidatedResponse(testCase, HttpMethod.PUT, body).assertThat()
                .contentType(testCase.getResponseContentType())
                .statusCode(testCase.getStatusCode());
    }

    /*
     * DELETE TESTS
     */
    public static void testDelete(EndpointTestCase testCase, Object body) {
        processResponseBody(
                getValidatedResponse(testCase, HttpMethod.DELETE, body).statusCode(testCase.getStatusCode()),
                testCase.getBodyValidationParams());
    }

    public static void testDelete_validateSchema(EndpointTestCase testCase, Object body) {
        getValidatedResponse(testCase, HttpMethod.DELETE, body).assertThat()
                .body(matchesJsonSchemaInClasspath(testCase.getSchemaLocation()));
    }

    public static void testDelete_validateResponseFormat(EndpointTestCase testCase, Object body) {
        getValidatedResponse(testCase, HttpMethod.DELETE, body).assertThat()
                .contentType(testCase.getResponseContentType())
                .statusCode(testCase.getStatusCode());
    }

    private static ValidatableResponse getValidatedResponse(EndpointTestCase testCase, String methodName) {
        return getValidatedResponse(testCase, methodName, null);
    }

    private static ValidatableResponse getValidatedResponse(EndpointTestCase testCase, String methodName, Object body) {
        Optional<Boolean> isCsrfEnabled = ConfigProvider.getConfig()
                .getOptionalValue(MicroprofilePropertyNames.CSRF_MODE, Boolean.class);

        RequestSpecification baseRequest;
        if (!testCase.getDisableCsrf() && isCsrfEnabled.isPresent() && isCsrfEnabled.get()) {
            baseRequest = addBodyIfPresent(AuthHelper.getCSRFDefinedResteasyRequest(), body, testCase);
        } else {
            baseRequest = addBodyIfPresent(given(), body, testCase);
        }

        switch (methodName) {
            case HttpMethod.POST:
                return baseRequest.accept(testCase.getRequestContentType())
                        .headers(testCase.getHeaderParams().orElse(Collections.emptyMap()))
                        .post(testCase.getPath(), testCase.getParams().orElse(new Object[] {})).then();
            case HttpMethod.PUT:
                return baseRequest.accept(testCase.getRequestContentType())
                        .headers(testCase.getHeaderParams().orElse(Collections.emptyMap()))
                        .put(testCase.getPath(), testCase.getParams().orElse(new Object[] {})).then();
            case HttpMethod.DELETE:
                return baseRequest.accept(testCase.getRequestContentType())
                        .headers(testCase.getHeaderParams().orElse(Collections.emptyMap()))
                        .delete(testCase.getPath(), testCase.getParams().orElse(new Object[] {})).then();
            case HttpMethod.GET:
            default:
                return baseRequest.accept(testCase.getRequestContentType())
                        .headers(testCase.getHeaderParams().orElse(Collections.emptyMap()))
                        .get(testCase.getPath(), testCase.getParams().orElse(new Object[] {})).then();
        }
    }

    /**
     * Accepts a ValidatableResponse object and a set of key-value pairs used for
     * testing the response body. Appends body(key, value) calls to the response if
     * the params object is not null.
     * 
     * @param response The response to process
     * @param params   the body validation params
     * @return The processed ValidatableResponse object
     */
    private static ValidatableResponse processResponseBody(ValidatableResponse response, Map<String, Object> params) {
        ValidatableResponse out = response;

        if (params != null) {
            for (Entry<String, Object> entry : params.entrySet()) {
                out = out.body(entry.getKey(), is(entry.getValue()));
            }
        }

        return out;
    }

    /**
     * We out of the box support JSON and have it mandated in our API best
     * practices. If the code breaks this pattern,
     * then you are either doing something wrong, should write more tailor-made
     * tests, or skip testing in the case of
     * HTML endpoints that are complicated to test.
     * 
     * @param spec     the current request spec in which the body should be added if
     *                 present
     * @param body     the body of the request to include, may be null
     * @param testCase the test case used to set the content type
     * @return the current request, wrapped with the json body content if it is set.
     */
    private static RequestSpecification addBodyIfPresent(RequestSpecification spec, Object body,
            EndpointTestCase testCase) {
        return body == null ? spec : spec.body(body).contentType(testCase.getRequestContentType());
    }

    /**
     * Represents test cases for a given endpoint, including path, parameters,
     * schema location, and some extra test
     * settings for specific cases.
     * 
     * @author Martin Lowe
     *
     */
    @AutoValue
    @JsonDeserialize(builder = AutoValue_RestAssuredTemplates_EndpointTestCase.Builder.class)
    public static abstract class EndpointTestCase {
        public abstract String getPath();

        public abstract Optional<Object[]> getParams();

        @Nullable
        public abstract String getSchemaLocation();

        public abstract Boolean getDisableCsrf();

        public abstract ContentType getRequestContentType();

        @Nullable
        public abstract ContentType getResponseContentType();

        public abstract Integer getStatusCode();

        @Nullable
        public abstract Map<String, Object> getBodyValidationParams();

        public abstract Optional<Map<String, Object>> getHeaderParams();

        public static Builder builder() {
            return new AutoValue_RestAssuredTemplates_EndpointTestCase.Builder().setDisableCsrf(false)
                    .setRequestContentType(ContentType.JSON).setStatusCode(200);
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setPath(String organizationId);

            public abstract Builder setParams(Optional<Object[]> params);

            public abstract Builder setSchemaLocation(@Nullable String schemaLocation);

            public abstract Builder setDisableCsrf(Boolean disableCsrf);

            public abstract Builder setRequestContentType(ContentType contentType);

            public abstract Builder setResponseContentType(@Nullable ContentType contentType);

            public abstract Builder setStatusCode(Integer statusCode);

            public abstract Builder setBodyValidationParams(@Nullable Map<String, Object> params);

            public abstract Builder setHeaderParams(Optional<Map<String, Object>> params);

            public abstract EndpointTestCase build();
        }
    }
}
