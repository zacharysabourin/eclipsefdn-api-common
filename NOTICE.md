# Notices for react-eclipsefdn-members

This content is produced and maintained by the Eclipse Foundation. Trademarks are the property of their respective owners.

* Project home: https://gitlab.eclipse.org/eclipsefdn/webdev/eclipsefdn-api-common

## Trademarks

* Eclipse® is a Trademark of the Eclipse Foundation, Inc.
* Eclipse Foundation is a Trademark of the Eclipse Foundation, Inc.

## Copyright

All content is the property of the respective authors or their employers. For
more information regarding authorship of content, please consult the listed
source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

* https://gitlab.eclipse.org/eclipsefdn/webdev/eclipsefdn-api-common

## Third-party Content

### Quarkus (^1.9.2)

* License: The Apache-2.0 License (APL-2.0)

### Apache commons-lang3 (^3.9)

* License: The Apache-2.0 License (APL-2.0)

### Lucene Core (8.5.2)

* License: The Apache-2.0 License (APL-2.0)

### Lucene Queries (8.5.2)

* License: The Apache-2.0 License (APL-2.0)

### Apache Solr Solrj (8.4.1)

* License: The Apache-2.0 License (APL-2.0)

### JBoss Log Manager (2.1.14.Final)

* License: The Apache-2.0 License (APL-2.0)
