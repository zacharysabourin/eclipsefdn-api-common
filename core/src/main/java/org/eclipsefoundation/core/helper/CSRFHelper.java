/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.helper;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.exception.FinalUnauthorizedException;
import org.eclipsefoundation.core.model.AdditionalUserData;
import org.eclipsefoundation.core.model.CSRFGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper class for interacting with CSRF tokens within the server. Generates secure CSRF tokens and compares them to
 * the copy that exists within the current session object.
 * 
 * @author Martin Lowe
 *
 */
@Singleton
public final class CSRFHelper {
    public static final Logger LOGGER = LoggerFactory.getLogger(CSRFHelper.class);

    @ConfigProperty(name = "security.csrf.enabled", defaultValue = "false")
    boolean csrfEnabled;
    @ConfigProperty(name = "security.csrf.enabled.distributed-mode", defaultValue = "false")
    Instance<Boolean> distributedMode;

    @Inject
    CSRFGenerator generator;

    /**
     * Generate a new CSRF token that has been hardened to make it more difficult to predict.
     *
     * @return a cryptographically-secure CSRF token to use in a session.
     */
    public String getNewCSRFToken(HttpServletRequest httpServletRequest) {
        return generator.getCSRFToken(httpServletRequest);
    }

    /**
     * Checks the current method of CSRF storage (session vs distributed) and returns the stored CSRF value.
     * 
     * @param aud the current local session data
     * @param httpServletRequest the request context
     * @return the CSRF token for the current request.
     */
    public String getSessionCSRFToken(AdditionalUserData aud, HttpServletRequest httpServletRequest) {
        if (distributedMode.get()) {
            return generator.getCSRFToken(httpServletRequest);
        } else {
            return aud.getCsrf();
        }
    }

    /**
     * Legacy method for comapring CSRF values using the session data object. This has been upgraded to be
     * {@link CSRFHelper#compareCSRF(String, String)} to better accommodate distributed mode options.
     * 
     * @param aud the session data for current user
     * @param passedToken the passed CSRF header data
     * @throws FinalUnauthorizedException when CSRF token is missing in the user data, the passed header value, or does
     * not match
     */
    @Deprecated(forRemoval = true)
    public void compareCSRF(AdditionalUserData aud, String passedToken) {
        compareCSRF(aud.getCsrf(), passedToken);
    }

    /**
     * Compares the passed CSRF token to the token for the current user session.
     *
     * @param expectedToken the stored CSRF reference value
     * @param passedToken the passed CSRF header data
     * @throws FinalUnauthorizedException when CSRF token is missing in the user data, the passed header value, or does
     * not match
     */
    public void compareCSRF(String expectedToken, String passedToken) {
        if (csrfEnabled) {
            LOGGER.debug("Comparing following tokens:\n{}\n{}", expectedToken == null ? null : expectedToken,
                    passedToken);
            if (expectedToken == null) {
                throw new FinalUnauthorizedException(
                        "CSRF token not generated for current request and is required, refusing request");
            } else if (passedToken == null) {
                throw new FinalUnauthorizedException("No CSRF token passed for current request, refusing request");
            } else if (!verifyConstantTime(expectedToken, passedToken)) {
                throw new FinalUnauthorizedException("CSRF tokens did not match, refusing request");
            }
        }
    }

    private static boolean verifyConstantTime(String expectedToken, String token) {
        if (expectedToken == token) {
            return true;
        }
        if (expectedToken == null || token == null) {
            return false;
        }
        return MessageDigest.isEqual(expectedToken.getBytes(StandardCharsets.US_ASCII),
                token.getBytes(StandardCharsets.US_ASCII));
    }

}
