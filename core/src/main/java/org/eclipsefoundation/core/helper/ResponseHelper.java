/*********************************************************************
* Copyright (c) 2019 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.helper;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;

import org.eclipsefoundation.core.model.Error;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.service.CachingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Helper class that transforms data into a response usable for the RESTeasy container. Uses Jackson serializer to
 * format the data into a hashable string.
 * 
 * @deprecated as this isn't being applied globally or used almost anywhere, we need a better way to do this. An issue
 * will be created to implement a response level filter to make this able to be applied globally as a configuration option,
 * rather than a service that needs to be called.
 * 
 * @author Martin Lowe
 *
 */
@Deprecated(forRemoval = true, since = "1.6.5")
@ApplicationScoped
public class ResponseHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResponseHelper.class);

    @Inject
    ObjectMapper mapper;
    @Inject
    CachingService cachingService;

    /**
     * Builds a response using passed data. Uses references to the caching service and the current request to add
     * information about ETags and Cache-Control headers.
     * 
     * @param id the ID of the object to be stored in cache
     * @param wrapper the query parameters for the current request
     * @param data the data to attach to the response
     * @param cachingService the cache for the current data request.
     * @return a complete response object for the given data and request.
     */
    public Response build(String id, RequestWrapper wrapper, MultivaluedMap<String, String> optParams, Object data,
            Class<?> type) {
        // set default cache control flags for API responses
        CacheControl cc = new CacheControl();
        cc.setNoStore(wrapper.isCacheBypass());

        if (!cc.isNoStore()) {
            cc.setMaxAge((int) cachingService.getMaxAge());
            // get the TTL for the current entry
            Optional<Long> ttl = cachingService.getExpiration(id, optParams == null ? wrapper.asMap() : optParams,
                    type);
            if (!ttl.isPresent()) {
                return Response.serverError().build();
            }

            // serialize the data to get an etag
            try {
                // ingest the content and hash to create an etag for current content
                MessageDigest digest = MessageDigest.getInstance("md5");
                digest.update(mapper.writeValueAsString(data).getBytes(StandardCharsets.UTF_8));
                String hash = DatatypeConverter.printHexBinary(digest.digest());

                // check if etag matches
                String etag = wrapper.getHeader("Etag");
                if (hash.equals(etag)) {
                    return Response.notModified(etag).cacheControl(cc).expires(new Date(ttl.get())).build();
                }
                // return a response w/ the generated etag
                return Response.ok(data).tag(hash).cacheControl(cc).expires(new Date(ttl.get())).build();
            } catch (JsonProcessingException e) {
                LOGGER.error("Error while serializing data of type " + type.getCanonicalName(), e);
                return new Error(500, "Error while generating output, cannot continue - RSP-001").asResponse();
            } catch (NoSuchAlgorithmException e) {
                LOGGER.error("MD5 algorithm can't be found, cannot hash content", e);
                return new Error(500, "Error while generating metadata, cannot continue - RSP-002").asResponse();
            }
        }
        return Response.ok(data).cacheControl(cc).build();
    }
}
