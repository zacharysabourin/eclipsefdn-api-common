/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.microprofile.config.ConfigProvider;

import io.undertow.util.HexConverter;

/**
 * Interface used to generate random hardened tokens for use in requests.
 * 
 * @author Martin Lowe
 *
 */
public interface CSRFGenerator {

    /**
     * Generates a random secure CSRF token to be used in requests. This token should be cryptographically secure and
     * random to ensure that it cannot be generated from an external source.
     * 
     * @param requestContext current request context that can be used for fingerprinting the request if required.
     * @return a random encoded string token
     */
    public String getCSRFToken(HttpServletRequest httpServletRequest);

    /**
     * Default implementation of the CSRF generator, will use secure randoms generated on the fly at runtime to generate
     * random seed values as base of token. These values will be hashed and salted to provide extra hardening of the
     * value to make it harder to predict.
     * 
     * @author Martin Lowe
     *
     */
    public static class DefaultCSRFGenerator implements CSRFGenerator {

        @Override
        public String getCSRFToken(HttpServletRequest httpServletRequest) {
            Optional<String> salt = ConfigProvider.getConfig().getOptionalValue("security.csrf.token.salt",
                    String.class);
            // use a random value salted with a configured static value
            String secureBase = UUID.randomUUID().toString();
            // create a secure random secret to embed in the user session
            String preHash = secureBase + salt.orElseGet(() -> "short-salt");

            // create new digest to hash the result
            MessageDigest md;
            try {
                md = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException e) {
                throw new IllegalStateException("Could not find SHA-256 algorithm to encode CSRF token", e);
            }
            // hash the results using the message digest
            byte[] array = md.digest(preHash.getBytes());
            // convert back to a hex string to act as a token
            return HexConverter.convertToHexString(array);
        }
    }
}
