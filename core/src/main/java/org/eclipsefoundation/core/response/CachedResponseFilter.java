/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.response;

import java.io.IOException;

import javax.annotation.Priority;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.service.CacheRecorderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.http.HttpMethod;

/**
 * Either applies recorded actions to the response or to record the response if nothing has been recorded yet.
 *
 * @author Martin Lowe
 */
@Provider
@Priority(50)
public class CachedResponseFilter implements ContainerResponseFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(CachedResponseFilter.class);

    @Inject
    Instance<CacheRecorderService> recorderService;
    @Inject
    RequestWrapper wrap;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
            throws IOException {
        // check if response status is in OK range and was a GET method
        LOGGER.trace("Checking request {}", wrap.getEndpoint());
        if (requestContext.getMethod().equalsIgnoreCase(HttpMethod.GET.name()) && responseContext.getStatus() >= 200
                && responseContext.getStatus() < 300) {
            recorderService.get().applyRecords(wrap, responseContext);
        }
    }
}
