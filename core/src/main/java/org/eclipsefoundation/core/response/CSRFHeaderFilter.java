/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.response;

import java.io.IOException;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.helper.CSRFHelper;
import org.eclipsefoundation.core.model.AdditionalUserData;
import org.eclipsefoundation.core.namespace.RequestHeaderNames;

/**
 * Injects the CSRF header token into the response when enabled for a server.
 *
 * @author Martin Lowe
 */
@Provider
public class CSRFHeaderFilter implements ContainerResponseFilter {
    @ConfigProperty(name = "security.csrf.enabled", defaultValue = "false")
    Instance<Boolean> csrfEnabled;
    @ConfigProperty(name = "security.csrf.enabled.distributed-mode", defaultValue = "false")
    Instance<Boolean> distributedMode;

    @Context
    HttpServletRequest httpServletRequest;

    @Inject
    CSRFHelper csrf;
    @Inject
    AdditionalUserData aud;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
            throws IOException {
        // only attach if CSRF is enabled for the current runtime
        if (csrfEnabled.get()) {
            // generate the token
            String token = csrf.getNewCSRFToken(httpServletRequest);
            // store token in session if not distributed mode
            if (aud.getCsrf() == null && !distributedMode.get()) {
                aud.setCsrf(token);
            }
            // attach the current CSRF token as a header on the request
            responseContext.getHeaders().add(RequestHeaderNames.CSRF_TOKEN, token);
        }
    }
}
