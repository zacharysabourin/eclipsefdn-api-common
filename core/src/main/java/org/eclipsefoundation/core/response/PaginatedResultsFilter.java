/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.response;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.annotation.Priority;
import javax.enterprise.inject.Instance;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.core.namespace.MicroprofilePropertyNames;
import org.jboss.resteasy.core.ResteasyContext;
import org.jboss.resteasy.spi.LinkHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Adds pagination and Link headers to the response by slicing the response entity if its a list entity. This will not
 * dig into complex entities to avoid false positives.
 *
 * @author Martin Lowe
 */
@Provider
@Priority(5)
public class PaginatedResultsFilter implements ContainerResponseFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(PaginatedResultsFilter.class);
    // should be set whenever we pass a limited subset to the response to be
    // paginated of a larger subset
    public static final String MAX_RESULTS_SIZE_HEADER = "X-Max-Result-Size";
    // should be set whenever we request data that has a maximum page size that is
    // different than the default here
    public static final String MAX_PAGE_SIZE_HEADER = "X-Max-Page-Size";

    @ConfigProperty(name = "eclipse.pagination.enabled", defaultValue = "true")
    Instance<Boolean> enablePagination;

    @ConfigProperty(name = MicroprofilePropertyNames.DEFAULT_PAGE_SIZE, defaultValue = "1000")
    Instance<Integer> defaultPageSize;
    // Force scheme of header links to be a given value, useful for proxied requests
    @ConfigProperty(name = "eclipse.pagination.scheme.enforce", defaultValue = "true")
    Instance<Boolean> enforceLinkScheme;

    @ConfigProperty(name = "eclipse.pagination.scheme.value", defaultValue = "https")
    Instance<String> linkScheme;

    @Context
    HttpServletResponse response;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
            throws IOException {
        if (enablePagination.get()) {
            Object entity = responseContext.getEntity();
            // only try and paginate if there are multiple entities
            if (entity instanceof Set) {
                paginateResults(responseContext,
                        (new TreeSet<>((Set<?>) entity)).stream().collect(Collectors.toList()));
            } else if (entity instanceof List) {
                paginateResults(responseContext, (List<?>) entity);
            }
        }
    }

    private void paginateResults(ContainerResponseContext responseContext, List<?> listEntity) {
        int pageSize = getCurrentLimit(responseContext);
        // if available, use max results header value
        String rawMaxSize = (String) getResponseHeader(responseContext, MAX_RESULTS_SIZE_HEADER);
        int maxSize = listEntity.size();
        if (StringUtils.isNumeric(rawMaxSize)) {
            maxSize = Integer.valueOf(rawMaxSize);
        }
        int page = getRequestedPage(maxSize, pageSize);

        int lastPage = (int) Math.ceil((double) maxSize / pageSize);
        // slice if the results set is larger than page size
        if (listEntity.size() > pageSize) {
            // set the sliced array as the entity
            responseContext
                    .setEntity(listEntity.subList(getArrayLimitedNumber(listEntity, Math.max(0, page - 1) * pageSize),
                            getArrayLimitedNumber(listEntity, pageSize * page)));
        }
        // set the link header to the response
        responseContext.getHeaders().add("Link", createLinkHeader(page, lastPage));
    }

    /**
     * Gets a header value if available, checking first the servlet response then the mutable response wrapper.
     * 
     * @param responseContext the mutable response wrapper
     * @param headerName the header to retrieve
     * @return the header value if set, null otherwise.
     */
    private String getResponseHeader(ContainerResponseContext responseContext, String headerName) {
        String rawHeaderVal = (String) response.getHeader(headerName);
        if (rawHeaderVal == null) {
            rawHeaderVal = responseContext.getHeaderString(headerName);
        }
        return rawHeaderVal;
    }

    private LinkHeader createLinkHeader(int page, int lastPage) {
        // add link headers for paginated page hints

        UriBuilder builder = getUriInfo().getRequestUriBuilder();
        LinkHeader lh = new LinkHeader();
        // add first + last page link headers
        lh.addLink("this page of results", "self", buildHref(builder, page), "");
        lh.addLink("first page of results", "first", buildHref(builder, 1), "");
        lh.addLink("last page of results", "last", buildHref(builder, lastPage), "");
        // add next/prev if needed
        if (page > 1) {
            lh.addLink("previous page of results", "prev", buildHref(builder, page - 1), "");
        }
        if (page < lastPage) {
            lh.addLink("next page of results", "next", buildHref(builder, page + 1), "");
        }
        return lh;
    }

    /**
     * Gets the current requested page, rounding down to max if larger than the max page number, and up if below 1.
     *
     * @param listEntity list entity used to determine the number of pages present for current call.
     * @return the current page number if set, the last page if greater, or 1 if not set or negative.
     */
    private int getRequestedPage(int maxSize, int pageSize) {
        MultivaluedMap<String, String> params = getUriInfo().getQueryParameters();
        if (params.containsKey(DefaultUrlParameterNames.PAGE.getName())) {
            try {
                int page = Integer.parseInt(params.getFirst(DefaultUrlParameterNames.PAGE.getName()));
                // use double cast int to allow ceil call to round up for pages
                int maxPage = (int) Math.ceil((double) maxSize / pageSize);

                // get page, with min of 1 and max of last page
                return Math.min(Math.max(1, page), maxPage);
            } catch (NumberFormatException e) {
                // page isn't a number, just return
                LOGGER.error("Passed bad page value: {}", params.getFirst("page"));
                return 1;
            }
        }
        return 1;
    }

    /**
     * Allows for external bindings to affect the current page size, defaulting to the internal set configuration.
     * 
     * @param responseContext
     * @return
     */
    private int getCurrentLimit(ContainerResponseContext responseContext) {
        // check the pagesize param
        MultivaluedMap<String, String> params = getUriInfo().getQueryParameters();
        if (params.containsKey(DefaultUrlParameterNames.PAGESIZE.getName())
                && StringUtils.isNumeric(params.getFirst(DefaultUrlParameterNames.PAGESIZE.getName()))) {
            int pageSize = Integer.parseInt(params.getFirst(DefaultUrlParameterNames.PAGESIZE.getName()));
            int maxPageSize = getInternalMaxPageSize();
            return Math.min(pageSize, maxPageSize);
        }
        return getInternalMaxPageSize();
    }

    /**
     * Returns the max page size as defined by internal metrics (ignoring pagesize params).
     * 
     * @return the internal max page size.
     */
    private int getInternalMaxPageSize() {
        if (response.getHeaderNames().contains(MAX_PAGE_SIZE_HEADER)) {
            try {
                return Integer.parseInt(response.getHeader(MAX_PAGE_SIZE_HEADER));
            } catch (NumberFormatException e) {
                // page size isn't a number, allow to return default outside current scope
            }
        }
        return defaultPageSize.get();
    }

    /**
     * Builds an href for a paginated link using the BaseUri UriBuilder from the UriInfo object, replacing just the page
     * query parameter.
     *
     * @param builder base URI builder from the UriInfo object.
     * @param page the page to link to in the returned link
     * @return fully qualified HREF for the paginated results
     */
    private String buildHref(UriBuilder builder, int page) {
        if (enforceLinkScheme.get()) {
            return builder.scheme(linkScheme.get()).replaceQueryParam("page", page).build().toString();
        }
        return builder.replaceQueryParam("page", page).build().toString();
    }

    /**
     * Gets an int bound by the size of a list.
     *
     * @param list the list to bind the number by
     * @param num the number to check for exceeding bounds.
     * @return the passed number if its within the size of the given array, 0 if the number is negative, and the array
     * size if greater than the maximum bounds.
     */
    private int getArrayLimitedNumber(List<?> list, int num) {
        return Math.min(list.size(), Math.max(0, num));
    }

    private UriInfo getUriInfo() {
        return ResteasyContext.getContextData(UriInfo.class);
    }
}
