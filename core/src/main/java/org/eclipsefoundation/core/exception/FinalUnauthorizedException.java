/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.exception;

/**
 * Represents an unauthorized request with no redirect (standard UnauthorizedException gets routed
 * to OIDC login page when active, which is not desired).
 *
 * @author Martin Lowe
 */
public class FinalUnauthorizedException extends RuntimeException {

  public FinalUnauthorizedException(String message) {
    super(message);
  }

  /** */
  private static final long serialVersionUID = 1L;
}
