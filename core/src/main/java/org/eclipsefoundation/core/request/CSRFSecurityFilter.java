/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.request;

import java.io.IOException;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.exception.FinalUnauthorizedException;
import org.eclipsefoundation.core.helper.CSRFHelper;
import org.eclipsefoundation.core.model.AdditionalUserData;
import org.eclipsefoundation.core.namespace.MicroprofilePropertyNames;
import org.eclipsefoundation.core.namespace.RequestHeaderNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.undertow.httpcore.HttpMethodNames;

/**
 * Creates a security layer in front of mutation requests to require CSRF tokens (if enabled). This layer does not
 * perform the check of the token in-case there are other conditions that would rebuff the request.
 * 
 * @author Martin Lowe
 */
@Provider
public class CSRFSecurityFilter implements ContainerRequestFilter {
    public static final Logger LOGGER = LoggerFactory.getLogger(CSRFSecurityFilter.class);

    @ConfigProperty(name = MicroprofilePropertyNames.CSRF_MODE, defaultValue = "false")
    Instance<Boolean> csrfEnabled;
    @ConfigProperty(name = MicroprofilePropertyNames.DISTRIBUTED_CSRF_MODE, defaultValue = "false")
    Instance<Boolean> distributedMode;

    @Context
    HttpServletRequest httpServletRequest;
    @Inject
    Instance<CSRFHelper> csrf;
    @Inject
    AdditionalUserData aud;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        if (csrfEnabled.get()) {
            // check if the HTTP method indicates a mutation
            String method = requestContext.getMethod();
            if (HttpMethodNames.DELETE.equals(method) || HttpMethodNames.POST.equals(method)
                    || HttpMethodNames.PUT.equals(method)) {
                // check csrf token presence (not value)
                String token = requestContext.getHeaderString(RequestHeaderNames.CSRF_TOKEN);
                if (token == null || "".equals(token.trim())) {
                    throw new FinalUnauthorizedException("No CSRF token passed for mutation call, refusing connection");
                } else if (distributedMode.get()) {
                    // distributed mode should return the stored token if it exists
                    csrf.get().compareCSRF(csrf.get().getNewCSRFToken(httpServletRequest), token);
                } else {
                    // run comparison. If error, exception will be thrown
                    csrf.get().compareCSRF(aud.getCsrf(), token);

                }
            }
        }
    }
}
