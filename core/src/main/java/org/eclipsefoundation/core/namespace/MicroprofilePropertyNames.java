/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.namespace;

/**
 * Contains Microprofile property names used by this application.
 * 
 * @author Martin Lowe
 *
 */
public class MicroprofilePropertyNames {
    public static final String CACHE_TTL_MAX_SECONDS = "cache.ttl.write.seconds";
    public static final String CACHE_SIZE_MAX = "cache.max.size";
    public static final String MULTI_SOURCE_SINGLE_TENANT_ENABLED = "eclipse.persistence.msst.enabled";
    public static final String DEFAULT_PAGE_SIZE = "eclipse.pagination.page-size.default";
    public static final String CSRF_MODE = "security.csrf.enabled";
    public static final String DISTRIBUTED_CSRF_MODE = "security.csrf.enabled.distributed-mode";
    public static final String OPTIONAL_RESOURCES_ENABLED = "eclipse.optional-resources.enabled";
    public static final String CACHE_RESOURCE_ENABLED = "eclipse.cache.resource.enabled";

    private MicroprofilePropertyNames() {
    }
}
