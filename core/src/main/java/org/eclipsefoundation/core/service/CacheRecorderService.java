/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.service;

import javax.ws.rs.container.ContainerResponseContext;

import org.eclipsefoundation.core.model.RequestWrapper;

/**
 * Manages the recording and application of cache recorders to a request.
 * 
 * @author Martin Lowe
 *
 */
public interface CacheRecorderService {
    void recordRequest(RequestWrapper wrap);

    void applyRecords(RequestWrapper wrap, ContainerResponseContext context);
}
