/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.service.impl;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.namespace.MicroprofilePropertyNames;
import org.eclipsefoundation.core.service.CachingService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.cache.Cache;
import io.quarkus.cache.CacheInvalidate;
import io.quarkus.cache.CacheInvalidateAll;
import io.quarkus.cache.CacheKey;
import io.quarkus.cache.CacheName;
import io.quarkus.cache.CacheResult;
import io.quarkus.cache.CaffeineCache;

/**
 * Utililzes Quarkus caching extensions in order to cache and retrieve data.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class QuarkusCachingService implements CachingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(QuarkusCachingService.class);

    @ConfigProperty(name = MicroprofilePropertyNames.CACHE_SIZE_MAX, defaultValue = "10000")
    long maxSize;
    @ConfigProperty(name = MicroprofilePropertyNames.CACHE_TTL_MAX_SECONDS, defaultValue = "900")
    long ttlWrite;

    @Inject
    @CacheName("default")
    Cache cache;

    @Override
    public <T> Optional<T> get(String id, MultivaluedMap<String, String> params, Class<?> rawType,
            Callable<? extends T> callable) {
        Objects.requireNonNull(callable);

        ParameterizedCacheKey cacheKey = new ParameterizedCacheKey(rawType, id,
                params != null ? params : new MultivaluedMapImpl<>());
        LOGGER.debug("Retrieving cache value for '{}'", cacheKey);
        return Optional.ofNullable(get(cacheKey, callable));
    }

    @Override
    public Optional<Long> getExpiration(String id, MultivaluedMap<String, String> params, Class<?> type) {
        ParameterizedCacheKey cacheKey = new ParameterizedCacheKey(type, id, params);
        try {
            return Optional.ofNullable(getExpiration(true, cacheKey));
        } catch (Exception e) {
            throw new RuntimeException("Error while retrieving expiration for cachekey: " + cacheKey.toString());
        }
    }

    @Override
    public Set<ParameterizedCacheKey> getCacheKeys() {
        return cache.as(CaffeineCache.class).keySet().stream().map(o -> {
            if (o instanceof ParameterizedCacheKey) {
                return (ParameterizedCacheKey) o;
            } else {
                // dont record broken keys, and filter null values out
                LOGGER.warn("Found illegal cache key value in default cache: {}", o.toString());
                return null;
            }
        }).dropWhile(Predicate.isEqual(null)).collect(Collectors.toSet());

    }

    @Override
    @CacheInvalidate(cacheName = "default")
    @CacheInvalidate(cacheName = "ttl")
    public void remove(@CacheKey ParameterizedCacheKey key) {
        LOGGER.debug("Removed cache key '{}' from TTL and default cache regions", key);
    }

    @Override
    public void fuzzyRemove(String id, Class<?> type) {
        this.getCacheKeys().stream().filter(k -> k.getId().equals(id) && k.getClazz().equals(type))
                .forEach(this::remove);
    }

    @Override
    @CacheInvalidateAll(cacheName = "default")
    @CacheInvalidateAll(cacheName = "ttl")
    public void removeAll() {
        LOGGER.debug("Cleared TTL and default cache regions");
    }

    @Override
    public long getMaxAge() {
        return TimeUnit.MILLISECONDS.convert(ttlWrite, TimeUnit.SECONDS);
    }

    /**
     * Soft check of expiration, does not create new entries if no entry is present.
     * 
     * @param cacheKey key of cache item to retrieve TTL of.
     * @return the epoch time that the given cache key expires, or null if the key is not set.
     */
    @SuppressWarnings("java:S3516") // suppressed as the cached entry can be returned when present
    public Long checkExpiration(ParameterizedCacheKey cacheKey) {
        try {
            // check for existing value, throwing out if none is found.
            return getExpiration(true, cacheKey);
        } catch (Exception e) {
            // no result found
            return null;
        }
    }

    @CacheResult(cacheName = "default")
    public <T> T get(@CacheKey ParameterizedCacheKey cacheKey, Callable<? extends T> callable) {
        T out = null;
        try {
            out = callable.call();
            // set internal expiration cache
            getExpiration(false, cacheKey);
        } catch (Exception e) {
            LOGGER.error("Error while creating cache entry for key '{}': \n", cacheKey, e);
        }
        return out;
    }

    @CacheResult(cacheName = "ttl")
    public long getExpiration(boolean throwIfMissing, @CacheKey ParameterizedCacheKey cacheKey) throws Exception {
        if (throwIfMissing) {
            throw new Exception("No TTL present for cache key '{}', not generating");
        }
        LOGGER.debug("Timeout for {}: {}", cacheKey, System.currentTimeMillis() + getMaxAge());
        return System.currentTimeMillis() + getMaxAge();
    }

}
