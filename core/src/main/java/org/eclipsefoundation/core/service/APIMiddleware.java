/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.service;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import javax.annotation.Nullable;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;

public interface APIMiddleware {

    <T> List<T> paginationPassThrough(Function<BaseAPIParameters, Response> supplier, RequestWrapper request,
            Class<T> type);

    /**
     * Returns the full list of data for the given API call, using a function that accepts the page number to iterate
     * over the pages of data. The Link header is scraped off of the first request and is used to determine how many
     * calls need to be made to get the full data set.
     * 
     * @param <T> the type of data that is retrieved
     * @param supplier function that accepts a page number and makes an API call for the given page.
     * @param type class for the entity type returned for the response.
     * @return the full list of results for the given endpoint.
     */
    <T> List<T> getAll(Function<BaseAPIParameters, Response> supplier, Class<T> type);

    public static class BaseAPIParameters {
        private Integer page;
        private Integer limit;
        private RequestWrapper requestWrapper;

        public BaseAPIParameters() {
        }

        public BaseAPIParameters(@Nullable Integer page, @Nullable Integer limit,
                @Nullable RequestWrapper requestWrapper) {
            this.limit = limit;
            this.page = page;
            this.requestWrapper = requestWrapper;
        }

        /**
         * @return the page
         */
        @Nullable
        @QueryParam("page")
        public Integer getPage() {
            return page;
        }

        /**
         * @return the limit
         */
        @Nullable
        @QueryParam("pagesize")
        public Integer getLimit() {
            return limit;
        }

        /**
         * @return the requestWrapper
         */
        @Nullable
        public RequestWrapper getRequestWrapper() {
            return requestWrapper;
        }

        public static Builder builder() {
            return new Builder();
        }

        public static BaseAPIParameters buildFromWrapper(RequestWrapper request) {
            // set the params in the new wrap to be used
            Optional<String> page = request.getFirstParam(DefaultUrlParameterNames.PAGE);
            int pageActual = 1;
            if (page.isPresent() && StringUtils.isNumeric(page.get())) {
                pageActual = Integer.valueOf(page.get());
            }
            return BaseAPIParameters.builder().setPage(pageActual).setLimit(request.getPageSize()).setRequestWrapper(request)
                    .build();
        }

        @Override
        public String toString() {
            return "BaseAPIParameters{" + "page=" + page + ", " + "limit=" + limit + "}";
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (o instanceof APIMiddleware.BaseAPIParameters) {
                APIMiddleware.BaseAPIParameters that = (APIMiddleware.BaseAPIParameters) o;
                return (this.page == null ? that.getPage() == null : this.page.equals(that.getPage()))
                        && (this.limit == null ? that.getLimit() == null : this.limit.equals(that.getLimit()));
            }
            return false;
        }

        @Override
        public int hashCode() {
            int h$ = 1;
            h$ *= 1000003;
            h$ ^= (page == null) ? 0 : page.hashCode();
            h$ *= 1000003;
            h$ ^= (limit == null) ? 0 : limit.hashCode();
            return h$;
        }

        public static class Builder {
            private Integer page;
            private Integer limit;
            private RequestWrapper requestWrapper;

            /**
             * @param page the page to set
             */
            public Builder setPage(@Nullable Integer page) {
                this.page = page;
                return this;
            }

            /**
             * @param limit the limit to set
             */
            @QueryParam("pagesize")
            public Builder setLimit(@Nullable Integer limit) {
                this.limit = limit;
                return this;
            }

            /**
             * @param requestWrapper the requestWrapper to set
             */
            public Builder setRequestWrapper(@Nullable RequestWrapper requestWrapper) {
                this.requestWrapper = requestWrapper;
                return this;
            }

            public BaseAPIParameters build() {
                return new BaseAPIParameters(page, limit, requestWrapper);
            }
        }
    }
}