/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.service.impl;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerResponseContext;

import org.eclipsefoundation.core.model.CacheRecorder;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.service.CacheRecorderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.arc.Unremovable;

@ApplicationScoped
@Unremovable
public class DefaultCacheRecorder implements CacheRecorderService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultCacheRecorder.class);

    @Inject
    Instance<CacheRecorder<?>> recorders;

    @Override
    public void recordRequest(RequestWrapper wrap) {
        recorders.forEach(r -> r.record(r.generateKey(wrap), wrap));
    }

    @Override
    public void applyRecords(RequestWrapper wrap, ContainerResponseContext context) {
        recorders.forEach(r -> applyRecord(r, wrap, context));
    }

    private <T> void applyRecord(CacheRecorder<T> r, RequestWrapper wrap, ContainerResponseContext context) {
        String key = r.generateKey(wrap);
        LOGGER.trace("Checking key '{}' for recorded metadata", key);
        if (r.hasRecorded(key)) {
            LOGGER.trace("Applying recorder '{}'", r.getClass().getSimpleName());
            r.apply(r.record(key, wrap), wrap, context);
        } else {
            LOGGER.trace("Generating recorder entry '{}'", r.getClass().getSimpleName());
            r.record(key, wrap);
        }
    }
}
