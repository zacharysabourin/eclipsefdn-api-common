/*********************************************************************
* Copyright (c) 2019 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.service.impl;

import java.io.InputStream;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.response.PaginatedResultsFilter;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.core.service.CacheRecorderService;
import org.jboss.resteasy.spi.LinkHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.arc.Unremovable;

/**
 * Serves as a middleware for external calls. This generalizes retrieving multiple pages of data using the Link header
 * rather than manually iterating over the data or using less robust checks (such as no data in response), or for
 * passing that pagination data on to the current request.
 * 
 * @author Martin Lowe, Zachary Sabourin
 *
 */
@ApplicationScoped
@Unremovable
public class DefaultAPIMiddleware implements APIMiddleware {
    public static final Logger LOGGER = LoggerFactory.getLogger(DefaultAPIMiddleware.class);
    private static final Pattern PAGE_QUERY_STRING_PARAM_MATCHER = Pattern.compile(".*[\\?&]?page=(\\d+).*");

    @Inject
    ObjectMapper objectMapper;
    @Inject
    CacheRecorderService recorderService;

    @Override
    public <T> List<T> paginationPassThrough(Function<BaseAPIParameters, Response> supplier, RequestWrapper request, Class<T> type) {
        // get the response for the current request
        LOGGER.trace("Making request with: {}", BaseAPIParameters.buildFromWrapper(request));
        Response r = supplier.apply(BaseAPIParameters.buildFromWrapper(request));
        // apply response headers forward (pass through)
        request
                .setHeader(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER,
                        r.getHeaderString(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        request.setHeader(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, r.getHeaderString(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));

        LOGGER
                .trace("Found pagination data max_size={} and page_size={} for request: {}",
                        r.getHeaderString(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER),
                        r.getHeaderString(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER), getURISafely(request.getURI()));
        // record the data to be used for this request
        recorderService.recordRequest(request);
        return readInValue(new LinkedList<>(), r, type);
    }

    @Override
    public <T> List<T> getAll(Function<BaseAPIParameters, Response> supplier, Class<T> type) {
        // get initial response
        int count = 1;
        Response r = supplier.apply(BaseAPIParameters.builder().setPage(count).build());
        // get the Link response header values
        int lastPage = getLastPageIndex(r);
        List<T> out = new LinkedList<>();
        readInValue(out, r, type);

        // iterate through all pages before returning results
        while (++count <= lastPage) {
            r = supplier.apply(BaseAPIParameters.builder().setPage(count).build());
            readInValue(out, r, type);
        }
        return out;
    }

    @SuppressWarnings({ "unchecked" })
    private <T> List<T> readInValue(List<T> out, Response r, Class<T> type) {
        Object o = r.getEntity();
        try {
            if (o instanceof InputStream
                    && (r.getHeaderString("Content-Encoding") != null && r.getHeaderString("Content-Encoding").equalsIgnoreCase("GZIP"))) {
                out.addAll(objectMapper.readerForListOf(type).readValue(new GZIPInputStream((InputStream) o)));
            } else if (o instanceof InputStream) {
                out.addAll(objectMapper.readerForListOf(type).readValue((InputStream) o));
            } else if (type.isAssignableFrom(o.getClass())) {
                out.add((T) o);
            } else if (o instanceof List) {
                out.addAll((List<T>) o);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return out;
    }

    private String getURISafely(URI uri) {
        if (uri == null) {
            return "";
        }
        return uri.toString();
    }

    private int getLastPageIndex(Response r) {
        List<Object> links = r.getHeaders().get("Link");
        // convert it to retrieve the index of the last page
        if (links != null && !links.isEmpty()) {
            LinkHeader h = LinkHeader.valueOf(links.get(0).toString());
            // check what the last page link has as its 'page' param val
            URI lastLink = h.getLinkByRelationship("last").getUri();
            Matcher m = PAGE_QUERY_STRING_PARAM_MATCHER.matcher(lastLink.getQuery());
            if (m.matches()) {
                return Integer.parseInt(m.group(1));
            }
        }
        return 0;
    }
}
