/*********************************************************************
* Copyright (c) 2019 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.service;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;

import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.helper.ParameterHelper;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;

import io.quarkus.arc.Arc;
import io.quarkus.arc.Unremovable;

/**
 * Interface defining the caching service to be used within the application.
 * 
 * @author Martin Lowe
 * @param <T> the type of object to be stored in the cache.
 */
@Unremovable
public interface CachingService {

    /**
     * Returns an Optional object of type T, returning a cached object if available, otherwise using the callable to
     * generate a value to be stored in the cache and returned.
     * 
     * @param id the ID of the object to be stored in cache
     * @param params the query parameters for the current request
     * @param callable a runnable that returns an object of type T
     * @param rawType the rawtype of data returned, which may be different than the actual data type returned (such as a
     * returned list containing the raw type).
     * @return the cached result
     */
    <T> Optional<T> get(String id, MultivaluedMap<String, String> params, Class<?> rawType,
            Callable<? extends T> callable);

    /**
     * Returns the expiration date in millis since epoch.
     * 
     * @param id the ID of the object to be stored in cache
     * @param params the query parameters for the current request
     * @return an Optional expiration date for the current object if its set. If there is no underlying data, then empty
     * would be returned
     */
    Optional<Long> getExpiration(String id, MultivaluedMap<String, String> params, Class<?> type);

    /**
     * @return the max age of cache entries
     */
    long getMaxAge();

    /**
     * Retrieves a set of cache keys available to the current cache.
     * 
     * @return unmodifiable set of cache entry keys.
     */
    Set<ParameterizedCacheKey> getCacheKeys();

    /**
     * Removes cache entry for given cache entry key.
     * 
     * @param key cache entry key
     */
    void remove(ParameterizedCacheKey key);

    /**
     * Removes cache entries with matching ID and type, regardless of the parameters for the cache key.
     * 
     * @param id id of entries to remove
     * @param type cached object type to be removed
     */
    void fuzzyRemove(String id, Class<?> type);

    /**
     * Removes all cache entries.
     */
    void removeAll();

    /**
     * Unique composite key objects, converted into a class for easier lookups, conversions, and comparisons
     */
    public static final class ParameterizedCacheKey {
        private final Class<?> clazz;
        private final String id;
        private final MultivaluedMap<String, String> params;

        public ParameterizedCacheKey(Class<?> clazz, String id, MultivaluedMap<String, String> params) {
            this.clazz = clazz;
            this.id = id;
            this.params = params;
        }

        public String getId() {
            return this.id;
        }

        public Class<?> getClazz() {
            return this.clazz;
        }

        /**
         * Creates a new map, puts all of the values into the new map, and returns the values.
         * 
         * @return a new map containing the params for the cache key
         */
        public MultivaluedMap<String, String> getParams() {
            MultivaluedMapImpl<String, String> out = new MultivaluedMapImpl<>();
            out.addAll((MultivaluedMapImpl<String, String>) this.params);
            return out;
        }

        @Override
        public int hashCode() {
            return Objects.hash(clazz, id, params);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            ParameterizedCacheKey other = (ParameterizedCacheKey) obj;
            return Objects.equals(clazz, other.clazz) && Objects.equals(id, other.id) && compareParams(other.params);
        }

        private boolean compareParams(MultivaluedMap<String, String> params) {
            if (params == null && this.params == null) {
                return true;
            } else if ((params == null && this.params != null) || (params != null && this.params == null)) {
                return false;
            }

            // check if same size and same values
            if (params.size() == this.params.size() && params.keySet().containsAll(this.params.keySet())) {
                for (String key : this.params.keySet()) {
                    // check that the lists have the same values
                    if (!params.get(key).equals(this.params.get(key))) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append('[').append(this.clazz.getSimpleName()).append(']');
            sb.append("id:").append(id);

            // filter the parameters using a reference to the parameter helper
            if (this.params == null) {
                return sb.toString();
            }
            ParameterHelper paramHelper = Arc.container().instance(ParameterHelper.class).get();
            MultivaluedMap<String, String> filteredParams = paramHelper.filterUnknownParameters(params);
            // join all the non-empty params to the key to create distinct entries for
            // filtered values
            filteredParams.entrySet().stream().filter(e -> !e.getValue().isEmpty())
                    .map(e -> e.getKey() + '=' + String.join(",", e.getValue())).forEach(s -> sb.append('|').append(s));
            return sb.toString();
        }
    }
}
