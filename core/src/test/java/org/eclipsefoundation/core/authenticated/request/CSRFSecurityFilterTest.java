/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.authenticated.request;

import static io.restassured.RestAssured.given;

import org.eclipsefoundation.core.namespace.RequestHeaderNames;
import org.eclipsefoundation.core.test.AuthenticatedTestProfile;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.restassured.filter.session.SessionFilter;
import io.restassured.response.Response;

/**
 * Test the CSRF security filter which can block requests based on presence of CSRF token. This makes use of the
 * authenticated test profile to reduce complexity of testing other facets of the core lib that have no interactions
 * with security.
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
@TestProfile(AuthenticatedTestProfile.class)
class CSRFSecurityFilterTest {

    @Test
    void validateNoToken() {
        // expect rebuff as no CSRF token was passed
        given().when().get("/test").then().statusCode(403);
        given().when().post("/test").then().statusCode(403);
        given().when().put("/test").then().statusCode(403);
        given().when().delete("/test").then().statusCode(403);
    }

    @Test
    void validateWrongToken() {
        // do a good request to trigger the build of the header internally
        given().when().get("/test/unguarded").then().statusCode(200);
        // expect rebuff as no CSRF token was passed
        given().header(RequestHeaderNames.CSRF_TOKEN, "bad-header-value").when().get("/test").then().statusCode(403);
        given().header(RequestHeaderNames.CSRF_TOKEN, "bad-header-value").when().post("/test").then().statusCode(403);
        given().header(RequestHeaderNames.CSRF_TOKEN, "bad-header-value").when().put("/test").then().statusCode(403);
        given().header(RequestHeaderNames.CSRF_TOKEN, "bad-header-value").when().delete("/test").then().statusCode(403);
    }

    @Test
    void validateRightCSRFToken() {
        SessionFilter sessionFilter = new SessionFilter();
        // do a good request to trigger the build of the header internally
        Response r = given().filter(sessionFilter).when().get("/test/unguarded");
        String expectedHeader = r.getHeader(RequestHeaderNames.CSRF_TOKEN);

        // expect rebuff as no CSRF token was passed
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, expectedHeader).when().post("/test").then()
                .statusCode(200);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, expectedHeader).when().delete("/test")
                .then().statusCode(200);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, expectedHeader).when().put("/test").then()
                .statusCode(200);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, expectedHeader).when().get("/test").then()
                .statusCode(200);
    }
}
