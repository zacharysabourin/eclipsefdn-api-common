/*********************************************************************
* Copyright (c) 2019 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.core.test.models.TestModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class DefaultAPIMiddlewareTest {

    @Inject
    APIMiddleware middleware;
    
    @Inject
    ObjectMapper objectMapper;

    @Test
    public void getAll_success_GZIPContent() {

        List<TestModel> out = middleware.getAll(
                params -> Response.ok(new ByteArrayInputStream(getListAsByteArray())).header("Content-Encoding", "GZIP")
                        .build(),
                TestModel.class);

        Assertions.assertTrue(out.size() > 0);
    }

    private byte[] getListAsByteArray() {
        List<TestModel> list = new LinkedList<TestModel>();

        list.add(TestModel.builder().setName("Jim Bob").setMail("fake@mail.com").build());
        list.add(TestModel.builder().setName("Bim Job").setMail("false@mail.com").build());

        byte[] compressedData = null;

        try (ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
                GZIPOutputStream gzipStream = new GZIPOutputStream(byteStream)) {
 
            // Convert list to JSON String before compression
            gzipStream.write(objectMapper.writeValueAsString(list).getBytes());

            //Explicitly close GZIPStream or it won't fully write.
            gzipStream.close();

            compressedData = byteStream.toByteArray();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return compressedData;
    }
}
