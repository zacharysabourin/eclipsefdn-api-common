/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.helper;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Test suite for the ResponseHelper class.
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class ResponseHelperTest {

	@Inject
	ResponseHelper rh;

	@Test
	void constructorTest() {
		// available but not recommended (laggy)
		ResponseHelper inline = new ResponseHelper();
		// check that we can create the object
		Assertions.assertNotNull(inline);
		// check that injected object exists
		Assertions.assertNotNull(rh);
	}
}
